#Realizado por Carlos Gonzalez Rey de la rosa y Varelia Diaz
from flask import Flask, render_template

app = Flask(_name_)

@app.route ('/')
def saludar():
    return 'Dicionario de Slang Panameno! Carlos Gonzalez Carranza 305190688'

@app.route ("/")
def index():
    titulo = "dicionario"
    palabra = ["xopa","mopri","parking","yala vida"]
    significado = ["saludo","primo","fiesta","asombro"]
    return render_template("index.html", titulo=titulo, palabra=palabra, significadi=significado) 

if _name=="main_":
    app.run(debug=True)
