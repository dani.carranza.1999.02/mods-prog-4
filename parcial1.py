#realizados por Carlos gonzalez C 305190688.
import sqlite3
NOMBRE_DB = "INVENTARIO"

def conectar():
    return sqlite3.connect(NOMBRE_DB)

def crear_tablas():
    tablas = [
    """
     CREATE TABLE IF NOT EXISTS inventario(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            productos TEXT NOT NULL,
            descripcion TEXT NOT NULL
        );
    """
    ]
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    for tabla in tablas:
         cursor.execute(crear_tablas)

def principal():
    crear_tablas()
    menu = """
    a) Agregar nuevos productos
    b) Editar productos
    c) Eliminar productos existentes
    d) Ver listado de productos
    e) Buscar descripcion de productos
    f) Salir
    Elige: """
    
    eleccion = ""
    
    while eleccion != "f":
        eleccion = input(menu)
    if eleccion == "a":
            producto = input("Ingresa el producto: ")
            posible_producto = buscar_producto(producto)
            if posible_producto:
                print(f"El producto '{producto}' ya existe")  
            else:
                producto = input("Ingresa la descripcion del producto: ")
                agregar_producto(producto, descripcion)  
                print("Producto Agregado")
    if eleccion == "b":
        producto = input("Ingresa el producto que quieres editar: ")
            nuevo_producto = input("Ingrese la nueva descripcion: ")
            editar_producto(producto, nuevo_producto)
            print("Producto actualizado")
    if eleccion == "c":
            producto = input("Ingresa el producto a eliminar: ")
            eliminar_producto(producto)
    if eleccion == "d":
            producto = obtener_producto()
            print("=== Lista de productos ===")
    for producto in producto:
         print(producto[0])
    if eleccion == "e"
    producto = input("Ingrese el producto de la cual quiere saber la descripcion: ")
    producto = buscar_descripcion_producto(producto)
    if descripcion:
                print(f"La descripcion de '{producto}' es:\n{descripcion[0]}")
    else:
                print(f"Producto '{producto}' no encontrado")

def agregar_producto(producto, descripcion):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    sentencia = "INSERT INTO inventarios(producto, descripcion) VALUES (?, ?)"
    cursor.execute(sentencia, [producto, descripcion])
    conexion.commit()

def editar_producto(producto, nueva_descripcion):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    sentencia = "UPDATE inventarios SET descripcion = ? WHERE producto = ?"
    cursor.execute(sentencia, [nueva_descripcion, producto])
    conexion.commit()

def eliminar_producto(producto):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    sentencia = "DELETE FROM inventario WHERE producto = ?"
    cursor.execute(sentencia, [producto])
    conexion.commit()

def obtener_producto():
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    consulta = "SELECT producto FROM inventario"
    cursor.execute(consulta)
    return cursor.fetchall()

def buscar_descripcion_producto(producto):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    consulta = "SELECT descripcion FROM inventario WHERE producto = ?"
    cursor.execute(consulta, [producto])
    return cursor.fetchone()

if __name__ == '__main__':
    principal()
