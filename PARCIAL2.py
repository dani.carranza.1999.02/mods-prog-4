from flask import Flask,jsonify
from flask_cors import CORS, cross_origin


app = Flask(_name_) 

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
from datos import datos


@app.route('/ping',methods=['GET'])
def ping():
    return 'Pong!'

#Trae todos los campos
@app.route('/data',methods=['GET'])
@cross_origin()
def data():
    return jsonify({'data':datos})
#Trae la priemra vacunacion con su fecha, tambien trae la ultima vacunacion con su fecha
@app.route('/inicio',methods=['GET'])
@cross_origin()
def inicio():
    IncioVacunacion = []
    FinVacunacion = []
    
    for dato in datos:
        IncioVacunacion.append(dato['field25']) 
        FinVacunacion.append(dato['field64'])
  
    
    
    return jsonify(
        
        {
            'fecha_de_la_primera_vacunacion':IncioVacunacion[0],
            'primera_vacunacion_porcentaje':IncioVacunacion[1],
            'fecha_de_la_ultima_vacunacion':FinVacunacion[1],
            'ultima_vacunacion_porcentaje':FinVacunacion[0]
        })

#Trae los  10 ultimos vacunados
@app.route('/ultimosvacunados',methods=['GET'])
@cross_origin()
def ultimos10():


    ultimosVacunados = []
    fechaUltimosvacunados=[]
    
    i=0
    
    for dato in datos:
        if i==0:
            
            for x in range(54, 64):
                fechaUltimosvacunados.append(dato['field'+str(x)])
            
        
        if i==1:
            
             for x in range(54, 64):
                ultimosVacunados.append(dato['field'+str(x)])
                
        i=i+1      
    
    return jsonify(
        
        {
            
            'ultimas_10_vacunaciones_porcentaje':fechaUltimosvacunados,
            'fechas_de_las_10_ultimas_vacunaciones':ultimosVacunados,

        })
    
    
#Trae los  10 primeros vacunados
@app.route('/primerosvacunados',methods=['GET'])
@cross_origin()
def primeros10():
    
    primerosVacunados = []
    fechaPrimerosvacunados=[]
    
    i=0
    
    for dato in datos:
        if i==0:
            
            for x in range(25, 35):
                fechaPrimerosvacunados.append(dato['field'+str(x)])
            
        
        if i==1:
            
             for x in range(25, 35):
                primerosVacunados.append(dato['field'+str(x)])
                
        i=i+1      
    
    return jsonify(
        
        {
            
            'primeros_10_vacunaciones_porcentaje':primerosVacunados,
            'fechas_de_los_10_primeras_vacunaciones':fechaPrimerosvacunados,

        })

if _name_ == '_main_':
    app.run(debug=True,port=4000)
