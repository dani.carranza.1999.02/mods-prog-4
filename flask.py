#Realizado por Carlos Gonzalez Rey de la rosa y Valeria DIaz
from flask import Flask

app = Flask(__name__)

@app.route ('/')
def saludar():
    return 'Hola, Profesor! a continuacion algunas palabras del diccionario!'

@app.route ('diccionario')
def diccionario(diccionario):
    return 'xopa: hola como estas, parking: Fiesta, yalabetia: algo exagerado, fren: amigo'

@app.route('/hola/<nombre>')
def saludar_nombre(nombre):
    return 'Hola %s!' % nombre

if __name__ == '__main__':
    app.run()
