#realizado por Carlos Gonzalez Rey de la rosa y Valeria Diaz
from pymongo import MongoClient
from pprint  import pprint

NOMBRE_BASE_DE_DATOS = "diccionario"
USUARIO = "root"
PALABRA_SECRETA = ""
conexion = MySQLDatabase(NOMBRE_BASE_DE_DATOS,
                         user=USUARIO, password=PALABRA_SECRETA)


class ModeloBase(Model):
    class Meta:
        database = conexion


class Palabra(ModeloBase):
    palabra = TextField()
    significado = TextField()


def crear_tablas():
    conexion.connect()
    conexion.create_tables([Palabra])


def principal():
    crear_tablas()
    menu = """
a) Agregar nueva palabra
b) Editar palabra existente
c) Eliminar palabra existente
d) Ver listado de palabras
e) Buscar significado de palabra
f) Salir
Elige: """
    eleccion = ""
    while eleccion != "f":
        eleccion = input(menu)
        if eleccion == "a":
            palabra = input("Ingresa la palabra: ")
            
            posible_significado = buscar_significado_palabra(palabra)
            if posible_significado:
                print(f"La palabra '{palabra}' ya existe")
            else:
                significado = input("Ingresa el significado: ")
                agregar_palabra(palabra, significado)
                print("Palabra agregada")
        if eleccion == "b":
            palabra = input("Ingresa la palabra que quieres editar: ")
            nuevo_significado = input("Ingresa el nuevo significado: ")
            editar_palabra(palabra, nuevo_significado)
            print("Palabra actualizada")
        if eleccion == "c":
            palabra = input("Ingresa la palabra a eliminar: ")
            eliminar_palabra(palabra)
        if eleccion == "d":
            palabras = obtener_palabras()
            print("=== Lista de palabras ===")
            for palabra in palabras:
               
                print(palabra.palabra)
        if eleccion == "e":
            palabra = input(
                "Ingresa la palabra de la cual quieres saber el significado: ")
            significado = buscar_significado_palabra(palabra)
            if significado:
                print(
                    f"El significado de '{palabra}' es:\n{significado}")
            else:
                print(f"Palabra '{palabra}' no encontrada")


def agregar_palabra(palabra, significado):
    Palabra.create(palabra=palabra, significado=significado)


def editar_palabra(palabra, nuevo_significado):
    Palabra.update({Palabra.significado: nuevo_significado}).where(
        Palabra.palabra == palabra).execute()


def eliminar_palabra(palabra):
    Palabra.delete().where(Palabra.palabra == palabra).execute()


def obtener_palabras():
    return Palabra.select()


def buscar_significado_palabra(palabra):
    
    try:
        return Palabra.select().where(Palabra.palabra == palabra).get().significado
    except Exception:
        
        return None


if __name__ == '__main__':
    principal()

client = MongoClient(mongodb://3.236.125.65:27017)
with client:
    db = client.significados
    db.significados.drops
  
    db= client.significado
        significados= db.significado.find ({"significado"})
        for cc in significado
            print(cc[significado])
